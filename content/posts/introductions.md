---
title: "We did it! Welcome to asgpodcast.com!"
date: 2020-01-25T21:05:45-05:00
draft: false
---

Welcome to the new home of the All Systems Go podcast show notes! This is Chris. We've been working through the things - Kofi and I are working through the best model to collaborate on these shownotes, and use this site for our thoughts as we get deeper into the Go language and overall app deployment/code lifecycle. 

This site is built using [Hugo](https://gohugo.io/) and it has turned out to be great to get started. We decided to use it because a) it's written in Go and b) there are a lot of great themes for Hugo. We settled on the Terminal theme after I deployed it one Saturday morning at one of the kids's baseball practice. The getting started page was so good that it was up and running fast. 

At a high level, the process was: 

1. Install Hugo
2. Pick a theme and install. This was the more difficult piece of the process. Running the command to install a theme is simple, but each theme has it's own set of configuration files that may be well documented or not. Kofi and I immediately settled on the [terminal](https://themes.gohugo.io/hugo-theme-terminal/) theme by Radek Kozieł. It's clean, nerdy and it was well documented. 
3. Generate the static pages. It's actually not clear to me right now if this is required now, since we are using an automated deployment. Hugo is a static site generator, so it takes a bunch of source files and turns them into web pages. Right now, I'm writing this in vscode in Markdown. Hugo will take this .md file and generate a web page based on the theme and configuration we have set. 
4. Create a repo somewhere. I use GitLab. It's great. For our site, since both Kofi and I really want to get to a place where listeners are viewing our code and contributing some of their own, I made a new Group "All Systems Go" in GitLab. In this case when I say GitLab I mean gitlab.com. I then created a project in that group, called "asg-web", and pushed all the files there to seed the website. 
5. Specific to GitLab, create a .gitlab-ci.yml file. This file tells the GitLab CI tooling that there is some business to do when new code appears. The Hugo ["Hosting & Deployment"](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/) docs do a good job describing the required config. GitLab isn't the only place to deploy. See the docs for the others. Once this file is in place, and pushed to the repo GitLab will build the site and host it in GitLab Pages. For free! 
6. (optional) Define your custom domain. I registered asgpodcast.com before we even recorded the first episode, so once the site was up I absolutely wanted to point the domain at the GitLab pages site. This step wasn't as clear to me. From the repo page on gitlab.com, go down to Settings, and click pages. The "New Domain" button will allow you to add your domain. The tricky bit that took a little bit of thinking on my part was that once you create the verification TXT record, you actually need to create an A record that points your domain to the GitLab Pages IP. Once that was done, we were in business! 

So I hope you all enjoy the new digs for Show Notes, and check back for our blog posts now and again. Thanks for listening (and reading!). You can find us in the #allsystemsgo Gopher Slack, on Twitter @asgpodcast.  